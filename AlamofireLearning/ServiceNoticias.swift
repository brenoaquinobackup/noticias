//
//  ServiceNoticias.swift
//  AlamofireLearning
//
//  Created by Breno Aquino on 21/07/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol ServiceNoticiasDelegate {
    
    func getNoticiasSucess()
    func getNociciasFailure(errorMessage: String)
}

class ServiceNoticias {
    
    var delegate: ServiceNoticiasDelegate
    
    required init(delegate: ServiceNoticiasDelegate) {
        
        self.delegate = delegate
    }
    
    func getNoticias() {
        
        RequestFactory.getNoticias().validate().responseArray { (response: DataResponse<[Noticia]>) in
            
            switch response.result {
                
            case .success:
                
                if let noticias = response.value {
                    
                    Noticia.saveAll(noticias: noticias)
                    
                    self.delegate.getNoticiasSucess()
                }
                
            case .failure:
                
                if let error = response.error {
                    
                    self.delegate.getNociciasFailure(errorMessage: error.localizedDescription)
                }
            }
        }
    }
}
