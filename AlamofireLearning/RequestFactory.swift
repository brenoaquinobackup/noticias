//
//  RequestFactory.swift
//  AlamofireLearning
//
//  Created by Breno Aquino on 21/07/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation
import Alamofire

class RequestFactory {
    
    static func getNoticias() -> DataRequest {
        
        return Alamofire.request(baseURL + "noticias", method: .get)
    }
}
