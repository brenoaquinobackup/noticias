//
//  ViewController.swift
//  AlamofireLearning
//
//  Created by Breno Aquino on 19/07/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ServiceNoticiasDelegate {

    var noticias: Results<Noticia>!
    var service: ServiceNoticias!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noticias = Noticia.all()
        
        self.service = ServiceNoticias(delegate: self)
        
        self.service.getNoticias()
    }
    
    
    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.noticias.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_noticia", for: indexPath)
        
        if let titulo = cell.textLabel {
            
            titulo.text = self.noticias[indexPath.row].titulo
        }
        
        if let data = cell.detailTextLabel {
            
            data.text = self.noticias[indexPath.row].data
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "id_web" {
            
            if let destination = segue.destination as? ViewController_WebView {
                
                if let indexPath = tableView.indexPathForSelectedRow {
                    
                    destination.noticia = self.noticias[indexPath.row]
                }
            }
        }
    }
    
    
    // MARK: Requests
    
    func getNoticiasSucess() {
        
        self.noticias = Noticia.all()
        
        self.tableView.reloadData()
    }
    
    func getNociciasFailure(errorMessage: String) {
        
        let alert = UIAlertController(title: "Opa!", message: errorMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

}

