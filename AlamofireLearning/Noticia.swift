//
//  Noticia.swift
//  AlamofireLearning
//
//  Created by Breno Aquino on 20/07/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Noticia: Object, Mappable {
    
    dynamic var id:Int = 0
    dynamic var data: String = ""
    dynamic var url: String = ""
    dynamic var titulo: String = ""
    dynamic var corpo: String = ""
    dynamic var tipo: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id      <-  map["id"]
        data    <-  map["dataPublicacao"]
        url     <-  map["urlImg"]
        titulo  <-  map["titulo"]
        corpo   <-  map["corpo"]
        tipo    <-  map["tipo"]
    }
}

extension Noticia {
    
    static func saveAll(noticias: [Noticia]) {
        
        try! realmInstance.write {
            
            realmInstance.add(noticias, update: true)
        }
    }
    
    static func all() -> Results<Noticia> {
        
        return realmInstance.objects(Noticia.self)
    }
}
