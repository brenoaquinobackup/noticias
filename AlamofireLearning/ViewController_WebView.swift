//
//  ViewController_WebView.swift
//  AlamofireLearning
//
//  Created by Breno Aquino on 20/07/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ViewController_WebView: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    
    var noticia: Noticia!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: noticia.url)
        let data = try? Data(contentsOf: url!)
        imgView.image = UIImage(data: data!)
        
        
        webView.loadHTMLString(noticia.corpo, baseURL: nil)
    }

}
